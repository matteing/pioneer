from flask import Flask, request, render_template, url_for, jsonify, json
from .search import search as search_backend
import os

app = Flask(__name__, static_url_path='/static')

ROOT = 'preprocessed/'
SEARCHABLE_ROOT = 'preprocessed/lean/'
FULL_ROOT = 'preprocessed/full/'

@app.route("/")
def home():
    return render_template('index.html')

@app.route("/search")
def search():
    terms = request.args.get('q')
    return jsonify(search_backend(terms))

@app.route('/email/<id>')
def email(id):
    path = os.path.join(FULL_ROOT, "%s.json" % id)
    if os.path.isfile(path):
        file = open(path, 'r', encoding='latin-1')
        content = json.loads(file.read())
        file.close()
        return content['content']
    else:
        return "Not found."