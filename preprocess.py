import os, pprint, hashlib, json, email, re
from nltk.corpus import stopwords

# Walk through the maildir's directories.
root = 'maildir/'
preprocessed_root = 'preprocessed/'
previewed = False
count = 0
skipped = 0

for dirpath, dirnames, filenames in os.walk(root):
    for filename in filenames:
        # Open the file.
        path = os.path.join(dirpath, filename)
        file = open(path, 'r', encoding='latin-1')
        # Get header metadata for email.
        metadata = {
            'id': None,
            'date': None,
            'sender': None,
            'subject': None,
            'folder': None,
            'to': None
        }
        content = file.read()

        message = email.message_from_string(content)

        # Close that file.
        file.close()

        mid = message.get('Message-ID', '')
        metadata['id'] = hashlib.md5(mid.encode('utf-8')).hexdigest()

        metadata['date'] = message.get('Date')
        metadata['sender'] = message.get('From')
        metadata['subject'] = message.get('Subject')
        metadata['folder'] = message.get('X-Folder')
        if message.get('To'):
            metadata['to'] = [e.strip().lower() for e in message.get('To').split(',')]

        # Now clean up file contents.
        proc_string = message.get_payload()
        # Lowercase everything.
        proc_string = proc_string.lower()
        # Remove punctuation.
        proc_string = re.sub(r'[^\w\s]', '', proc_string)
        # Remove common english words.
        word_list = filter(lambda word: word not in stopwords.words('english'), proc_string.split())
        # Now build a searchable string.
        tokens = " ".join(list(word_list))

        # Build searchable lean dict.
        lean_dict = {
            'id': metadata['id'],
            'content': tokens
        }

        # Make a full data dict.
        full_dict = {
            'id': metadata['id'],
            'metadata': metadata,
            'content': message.get_payload()
        }

        # Test whether the data is correct.
        if not previewed:
            pprint.pprint(lean_dict)
            pprint.pprint(full_dict)
            i = input("Data ok? [Y/n]: ")
            if i.lower() == 'y':
                previewed = True
            else:
                # exit()
                pass

        # Write the metadata to a file.
        if metadata['id'] != None:
            name = preprocessed_root + 'lean/' + metadata['id'] + '.json'
            os.makedirs(os.path.dirname(name), exist_ok=True)
            with open(name, "w") as f:
                f.write(json.dumps(lean_dict))
                f.close()

            name = preprocessed_root + 'full/' + metadata['id'] + '.json'
            os.makedirs(os.path.dirname(name), exist_ok=True)
            with open(name, "w") as f:
                f.write(json.dumps(full_dict))
                f.close()
                
            count += 1
            print(count, " emails processed")
        else:
            skipped += 1
            print(skipped, " skipped")