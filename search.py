import json, os

ROOT = 'preprocessed/'
SEARCHABLE_ROOT = 'preprocessed/lean/'
FULL_ROOT = 'preprocessed/full/'

def search(terms):
    processed = 0
    returned = []
    for dirpath, dirnames, filenames in os.walk(SEARCHABLE_ROOT):
        for filename in filenames:
            path = os.path.join(dirpath, filename)
            file = open(path, 'r', encoding='latin-1')
            parsed = json.loads(file.read())
            if terms in parsed['content']:
                returned.append(parsed['id'])
            processed += 1
            file.close()
            del file

    print("Processed ", processed, " emails.")
    return returned

"""while True:
    print("Input search terms below. Enter q to exit.")
    term = input(">>> ")
    if term.lower() == 'q':
        exit()
    print(search(term))"""